#!/usr/bin/env python3
from enum import Enum
from logging import info
from json import load
import schedule
from time import sleep
import tweepy as tw


class Day(Enum):
    MONDAY = "mon"
    TUESDAY = "tue"
    WEDNESDAY = "wed"
    THURSDAY = "thu"
    FRIDAY = "fri"
    SATURDAY = "sat"
    SUNDAY = "sun"


CONFIG_FILE = "config.json"


def load_config() -> dict:
    with open(CONFIG_FILE, 'r') as f:
        return load(f)


def login(config: dict) -> tw.API:
    auth = tw.OAuthHandler(config["consumer_key"],
                           config["consumer_secret"])
    auth.set_access_token(config["access_token"],
                          config["access_token_secret"])
    return tw.API(auth)


def tweet(config: dict, day: Day) -> None:
    info("Task: tweet for day {}.".format(day.value))
    api = login(config)
    info("Login OK.")
    api.media_upload(config["videos"][day.value])
    info("Media upload OK.")


def main():
    config = load_config()
    # schedule.every().monday.at(config["time"]).do(tweet, config=config, day=Day.MONDAY)
    schedule.every().tuesday.at(config["time"]).do(tweet, config=config, day=Day.TUESDAY)
    schedule.every().wednesday.at(config["time"]).do(tweet, config=config, day=Day.WEDNESDAY)
    schedule.every().thursday.at(config["time"]).do(tweet, config=config, day=Day.THURSDAY)
    schedule.every().friday.at(config["time"]).do(tweet, config=config, day=Day.FRIDAY)
    # schedule.every().saturday.at(config["time"]).do(tweet, config=config, day=Day.SATURDAY)
    # schedule.every().sunday.at(config["time"]).do(tweet, config=config, day=Day.SUNDAY)
    while True:
        schedule.run_pending()
        sleep(30)


if __name__ == "__main__":
    main()
